﻿using System.Reactive;
using App.Views;
using ReactiveUI;

namespace App.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    public ReactiveCommand<Unit, Unit>? RunCountersCommand { get; }
    public SequentialExecutionView SequentialExecutionView { get; } = new();
    public SequentialExecutionViewModel SequentialExecutionViewModel { get; } = new();
    public ConcurrentExecutionView ConcurrentExecutionView { get; } = new();
    public ConcurrentExecutionViewModel ConcurrentExecutionViewModel { get; } = new();
    
    private const int MaxValue = 50;
    private const int Speed = 250;
    
    public MainWindowViewModel()
    {
        RunCountersCommand = ReactiveCommand.Create(OnClickBtnRun);

        SequentialExecutionViewModel.MaxValue = MaxValue;
        SequentialExecutionViewModel.Speed = Speed;
        
        ConcurrentExecutionViewModel.MaxValue = MaxValue;
        ConcurrentExecutionViewModel.Speed = Speed;
    }

    private void OnClickBtnRun()
    {
        if (SequentialExecutionViewModel.CurrentThread is { IsAlive: true })
        {
            return;
        }
        
        if (ConcurrentExecutionViewModel.CurrentThread is { IsAlive: true })
        {
            return;
        }
        
        SequentialExecutionViewModel.OnClickBtnRun();
        ConcurrentExecutionViewModel.OnClickBtnRun();
    }
}
