using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Avalonia.Threading;
using ReactiveUI;

namespace App.ViewModels;

public class ConcurrentExecutionViewModel : ViewModelBase
{
    private ConcurrentQueue<Thread?> Queue { get; } = new();
    private Stopwatch Timer { get; } = new();
    public Thread? CurrentThread { get; private set; }
    public string? Counter1
    {
        get => _counter1;
        private set => this.RaiseAndSetIfChanged(ref _counter1, value);
    }
    public string? Counter2
    {
        get => _counter2;
        private set => this.RaiseAndSetIfChanged(ref _counter2, value);
    }
    public string? Counter3
    {
        get => _counter3;
        private set => this.RaiseAndSetIfChanged(ref _counter3, value);
    }
    public string? ExecutionTime
    {
        get => _executionTime;
        private set => this.RaiseAndSetIfChanged(ref _executionTime, value);
    }
    public int MaxValue { get; set; }
    public int Speed { get; set; }
    
    private string? _counter1 = "0";
    private string? _counter2 = "0";
    private string? _counter3 = "0";
    private string? _executionTime;    

    private void BuildThreads()
    {
        Queue.Enqueue(new Thread(() => RunCounter(value => Counter1 = value)));
        Queue.Enqueue(new Thread(() => RunCounter(value => Counter2 = value)));
        Queue.Enqueue(new Thread(() => RunCounter(value => Counter3 = value)));
    }
    
    public async void OnClickBtnRun()
    {
        ResetCounters();
        BuildThreads();
    
        Timer.Start();
    
        while (Queue.Count > 0)
        {
            if (Queue.TryDequeue(out Thread? thread))
            {
                CurrentThread = thread;
        
                await Task.Run(() =>
                {
                    CurrentThread?.Start();
                });
            }
        }
        
        await Task.Run(() =>
        {
            CurrentThread?.Join();
            Timer.Stop();
        });
    
        ExecutionTime = $@"Execution Time: {Timer.Elapsed:mm\:ss\.ff}";
    }

    private void RunCounter(Action<string> updateValue)
    {
        try
        {
            for (int i = 1; i <= MaxValue; i++)
            {
                int value = i;
            
                Dispatcher.UIThread.Invoke(() =>
                {
                    updateValue(value.ToString());
                });
                Thread.Sleep(Speed);
            }
        }
        catch (TaskCanceledException)
        {
            CurrentThread?.Interrupt();
        }
    }
    
    private void ResetCounters()
    {
        ExecutionTime = string.Empty;
        
        Counter1 = "0";
        Counter2 = "0";
        Counter3 = "0";
        
        Timer.Reset();
    }
}
